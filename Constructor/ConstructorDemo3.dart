

class Demo{
  int?x;
  String? str;

  Demo(int x,String str){
    this.x=x;
    this.str=str;
  }
  void printData(){
    print(x);
    print(str);
  }
}

void main(){
  
  Demo obj=new Demo(0,"v");
  obj.printData();

  print(obj.hashCode);            //same
  print(identityHashCode(obj));   //same

  Demo obj1= Demo(0,"v");
  print( obj1.hashCode);              // same
  print(identityHashCode(obj1));      //same

  
  print(  Demo(0,"v").hashCode);             // different
  print(identityHashCode( Demo(0,"v")));      // different

  print( new  Demo(0,"v").hashCode);             // different
  print(identityHashCode( new Demo(0,"v")));      // different



}