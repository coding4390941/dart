
//import 'dart:ffi';

class CenterGov{
  double ?cgdp;
  double  ?cbudget;
  CenterGov(this.cgdp,this.cbudget);

  void countryGDP(){
    print("Country GDP is : $cgdp");
  }
  void countryBudget(){
    print("Country Budget is: $cbudget");
  }
}
class StateGov extends CenterGov{
  //double ?sgdp;
  double ?sbudget;

  StateGov(this.sbudget,double cgdp ,double cbudget):super(cgdp,cbudget);

  void stateBudget(){
    print("State Budget is : $sbudget");
  }
}

void main(){
  StateGov obj=new StateGov(0.9,3.2,0.7);
  obj.countryBudget();
  obj.stateBudget();
}

