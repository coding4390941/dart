
//    Iterator on userdefined class ****
class ProgramLang implements Iterator{

  int ind=-1;
  var progLang =[];
  ProgramLang(String lang){

    this.progLang=lang.split(",");
  }

  bool moveNext(){
    if(ind<progLang.length-1){
      ind=ind+1;
      return true;
    }
    return false;
  }

  get current{
    if(ind>=0 && ind<=progLang.length-1) { 
      return progLang[ind];
    }
    }
  }

  void main(){

    ProgramLang obj=new ProgramLang("CPPP,JAVA,PYTHON,DART");

    while(obj.moveNext()){
      print(obj.current);
    }
  }