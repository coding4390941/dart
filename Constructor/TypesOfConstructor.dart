
// DEFAULT OR NO ARGUMENT
import 'dart:convert';

class Employee{
  int? empId;
  String?empName;
  Employee(){
     print(empId);
    print(empName);
    print("Default /no argu");

  }
}

// PARAMETERIZED

class Company{
  int? empId;
  String? empName;
  Company(this.empId,this.empName){
     print(empId);
    print(empName);
    print("Parameterized constructor");
  }
}


// NAMED CONSTRUCTOR

class Client{

  int? empId;
  String? empName;
  Client(){
    print(empId);
    print(empName);
    print("in default client constr");
  }

  Client.cons(int empId,String empName){
    print(empId);
    print(empName);
    print("In parameterzied client Constr");
  }
}
//  CONSTANT CONSTRUCTOR
class Player{
  final int?jerNo;      // Can't define a const constructor for a class with non-final fields
  final String? pName;

  const Player(this.jerNo,this.pName); // Const constructors can't have a body.

  void playerInfo(){
    print(jerNo);
    print(pName);
  }
}
void main(){
  Employee obj=new Employee();
  Company obj1=new Company(78, "Akshay");
  Client();
  Client.cons(10,"amar");
  Player obj2=new Player(7, "MSD");
  obj2.playerInfo();
}