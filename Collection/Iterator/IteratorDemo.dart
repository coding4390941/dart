
void main(){
/*
  var player =["Rohit","Shubhman","Rohit","KLRahul"];


//print
  print(player);
// for
for(var data in player){
  print(data);
}

// for each

player.forEach(print);

*/

var player =["Rohit","Shubhman","Rohit","KLRahul"];
print(player);
print(player.length);
print(player.runtimeType);
print(player.hashCode);

print(player.first);
print(player.last);

print(player.iterator);   //    Instance of 'ListIterator<String>'
print(player.iterator.runtimeType);   //  ListIterator<String>

var itr=player.iterator;

print(itr.moveNext());
print(itr.moveNext());
print(itr.moveNext());
print(itr.moveNext());
print(itr.moveNext());

}