class Company implements Iterator{

  int ind=-1;
  var empId=[];
  var empName=[];

  Company(String id,String name){
    this.empId=id.split(",");
    this.empName=name.split(",");
  }

  bool moveNext(){

    if(ind<empId.length-1){
      ind=ind+1;
      return true;
    }
    return false;
  }
    

    get current{
      if(ind>=0 && ind<=empId.length-1){
        return"${empId[ind]} :  ${empName[ind]}"; 
      }
    }
}

void main(){

  Company comp=new Company("1,2,3,4", "Amar,Akshay,Akash,Rahul");
  while(comp.moveNext()){
    print(comp.current);
  }
}