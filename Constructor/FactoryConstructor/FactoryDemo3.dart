abstract class Developer{

  factory Developer(String devType){

    if(devType =='Backend'){
      return Backend();
    }else if(devType =='FrontEnd'){
      return Frontend();
    }else if(devType =='Mobile'){
      return Mobile();
    }else{
      return Other();
    }
  }
    void devLang();    
}

class Backend implements Developer{
  void devLang(){
    print("NodeJS/SpringBoot/(PHP/Python)");
  }
}
class Frontend implements Developer{
  void devLang(){
    print("ReactJS/AngularJS");
  }
}
class Mobile implements Developer{
  void devLang(){
    print("Flutter/Android/Kotlin/Swift");
  }
}
class Other implements Developer{
  void devLang(){
    print("Testing/DevOps/Support");
  }
}


void main(){
  Developer obj1=new Developer("Frontend");
  Developer obj2=new Developer("Backend");
  Developer obj3=new Developer("Mobile");
  Developer obj4=new Developer("DevOps");

  obj1.devLang();
  obj2.devLang();
  obj3.devLang();
  obj4.devLang();
}
 
 //  not important 

 /* amar(){
    return "Amar";
  }
  print(amar);        // Closure: () => String
*/
