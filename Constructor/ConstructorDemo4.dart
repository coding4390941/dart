
class Company{
 int? empCount;
 String? compName;
 // ways tor write constructor not type
 Company(this.empCount,this.compName);

 void compInfo(){
  print(empCount);
  print(compName);
  print(this);
   } 
}

void main(){
  Company obj=new Company(100,"Veritas");
  Company obj1=new Company(200,"Pubmatic");

  obj.compInfo();
  obj1.compInfo();

}