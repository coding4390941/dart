import 'dart:collection';

void main(){

  var data=Queue();
  print(data); //  {}
  print(data.runtimeType); // ListQueue<dynamic>

  data.addAll([1,2,3]);
  data.addFirst(0);
  data.addLast(4);

  print(data); //{0, 1, 2, 3, 4}

  data.removeFirst();
  data.removeLast();

  print(data);  // {1, 2, 3}


}