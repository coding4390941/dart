
// object comst banavla aani value same asel tar kitipan object banvle tar same ch asto object 

// constant object  sathi 1) constructor const lagto  2) Instance Variable final lagtat(We cannot change value of variable)

class Demo{

  // static variable can be const or final
  static final  int? name;    // 
  static const int? age =21;  // 
  
  // instance variable only final
  final int? x ;
  final String? str;

//  const String? str ="";  //Only static fields can be declared as const

  const Demo(this.x,this.str);

}
void main(){
 
 //  Demo obj1 =  const new Demo(89,"Amar");  //   new' can't be used as an identifier because it's a keyword.
  
  Demo ob = new Demo(7,"Akshay");
  print(ob);
  print(ob.hashCode);

  Demo obj  = const  Demo(7,"Akshay");   // const obj sathi compalsary constructor constant lagto(with final variable)
  print(obj);
  print(obj.hashCode);     //  10000

  Demo obj1 = const  Demo(7,"Akshay");
  print(obj1.hashCode);    // 10000

  // Demo obj4 =   const final   Demo(7,"Akshay");  // (const final )-->'final' can't be used as an identifier because it's a keyword. (const final)--> Members can't be declared to be both 'const' and 'final'.

}