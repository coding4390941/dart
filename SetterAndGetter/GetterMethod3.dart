class Demo{
  int? _x;
  String? str;
  double? _sal;
  Demo(this._x,this.str,this._sal);

  get getX => _x;
  get getName => str;
  get getSal => _sal;  
  
  }
  /*
  or
  
  int? get getX => _x;
  String? get getName => str;
  double? get getSal =>_sal;
*/