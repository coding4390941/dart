import 'SetterMethod3.dart';

void main(){
  Demo obj=new Demo(45,"Rohit",6.5);
  obj.disp();
  
 // obj.setX(10); // The method 'setX' isn't defined for the type 'Demo'.
  obj.setX=10;
  obj.setName="rahul";
  obj.setSal=4.3;
  
  obj.disp();
 
}