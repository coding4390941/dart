class Demo{
  int x=10;
  int y=20;
  void add(int x){
    this.x=x;
    print(x);
  }
  void add(int x,int y){  // error : 'add' is already defined in class Demo
    this.x=x;
    this.y=y;
    print(x);
    print(y);
  }
}
void main(){
  Demo obj=new Demo();
  obj.add(100);
  obj.add(100,200);
}