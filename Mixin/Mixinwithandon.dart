mixin Demo1{
  void fun1(){
    print("In Demo1-fun1");
  }
}
mixin Demo2 on Demo1{    // mixin ha abstract class aahe 'on' kelyavr fakt method yetat tyala body dyavi lagte
  void fun2(){
    print("in Demo2-fun2");
  }
}
// class Normal extends Object with Demo2{  //Error: 'Object' doesn't implement 'Demo1' so it can't be used with 'Demo2'.
void main(){
//  Normal obj=new Normal();
//  obj.fun2();
}