
//  BY extending class

abstract class Developer{

  Developer(){}   // if not written then >>> error: The class 'Backend' can't extend 'Developer' because 'Developer' only has factory constructors (no generative constructors), and 'Backend' has at least one generative constructor.
  
    factory Developer.name(String devType){
  
      if(devType=='Backend'){
        return Backend();
      }else if(devType=='FrontEnd'){
        return Frontend();
      }else if(devType=='Mobile'){
        return Mobile();
      }else{
        return Other();
      }
    }
      void devLang();    
  }
  
  class Backend extends Developer{    // if  Developer(){} not written then error occur: >> The class 'Backend' can't extend 'Developer' because 'Developer' only has factory constructors (no generative constructors), and 'Backend' has at least one generative constructor.
    void devLang(){
      print("NodeJS/SpringBoot/(PHP/Python)");
    }
  }
  class Frontend extends Developer{
    void devLang(){
      print("ReactJS/AngularJS");
    }
  }
  class Mobile extends Developer{
    void devLang(){
      print("Flutter/Android/Kotlin/Swift");
    }
  }
  class Other extends Developer{
    void devLang(){
      print("Testing/DevOps/Support");
    }
  }
  
  
  void main(){
    Developer obj1=new Developer.name("FrontEnd");
    Developer obj2=new Developer.name("Backend");
    Developer obj3=new Developer.name("Mobile");
    Developer obj4=new Developer.name("DevOps");
  
    obj1.devLang();
    obj2.devLang();
    obj3.devLang();
    obj4.devLang();

    print(obj1.runtimeType);  //Frontend
    print(obj2.runtimeType);  //Backend
    print(obj3.runtimeType);  //Mobile
    print(obj4.runtimeType);  //Other
  }