

// final la runtime la value dili tar chalte , const la compiletime la dyavi lagte 

// const ha  compile time constant asto

// final ha runn time constant asto

import 'dart:io';
void main(){

  final int?x;
  final int?y;

 //  print(x); // The final variable 'x' can't be read because it's potentially unassigned at this point.
  
  y=1000;
  x=int.parse(stdin.readLineSync()!);
  
  print(x);
  print(y);

  // x=90; // The final variable 'x' can only be set once.
  // y=30; // The final variable 'y' can only be set once.


}