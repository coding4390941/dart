class Company{
  int empCount=500;
  String compName="Google";
  String loc="Pune";

  void compinfo(){
    print(empCount);
    print(compName);
    print(loc);

  }
}

void main(){
  Company obj1=new Company(); // 1
  obj1.compinfo();

  Company obj2=Company();     // 2
  obj2.compinfo();
  
  new Company();              // 3
  new Company().compinfo();
  
  Company().compinfo();       // 4
}


