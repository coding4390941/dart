/* 
const class Demo{    // Classes can't be declared to be 'const'. 
final  class Demo{   
  int x = 10;
  int y = 20;

 // const Demo(){   } //  Const constructors can't have a body.
 // final Demo(){   }  //  Can't have modifier 'final' here.

    Demo(){
    print("In Constructor");
  }
}
*/

class Demo{
  
//  name la value nahi dilyavr :--> Error: Final field 'name' is not initialized. Try to initialize the field in the declaration or in every constructor.

//  static final String? name;   // ethe constructor nahi lihla tar error yeto(The final variable 'name' must be initialized.) 
  static late final String? name;   
  static const int? age =21; 

 Demo(){
  print("IN CONSTRUCTOR:");
  name="Amar";
 }

//static final fun() {}      // Can't have modifier 'final' here. (same error for instance Methods)
//static const fun() {}      // Getters, setters and methods can't be declared to be 'const'.(Same error for instance Methods)

}
void main(){

//   print(Demo.name);     // Unhandled exception: LateInitializationError: Field 'name' has not been initialized.
  
  Demo obj = Demo();    // IN CONSTRUCTOR
  print(Demo.name);     // print --> Amar

//  Demo.name="Akshay";  //  Unhandled exception: LateInitializationError: Field 'name' has already been initialized.
//  print(Demo.name);

}