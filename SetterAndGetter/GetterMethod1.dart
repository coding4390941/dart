class Demo{
  int? _x;
  String? str;
  double? _sal;

  Demo(this._x,this.str,this._sal);

  int? getX(){
    return _x;
    }
  String? getName(){
    return str;
  }  
  double? getSal(){
    return _sal;
  }

  void disp(){
    print(_x);
    print(str);
    print(_sal);
  }
}