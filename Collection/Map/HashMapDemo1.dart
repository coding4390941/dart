import 'dart:collection';

void main(){

  var player =HashMap();
  //1
  player[18]="Virat";
  print(player);
  //2
  player.addAll({45:"Rohit"});
  print(player);
  //3


 // player.addEntries({7:"msd",1:"Rahul"}); // error: The argument type 'Map<int, String>' can't be assigned to the parameter type 'Iterable<MapEntry<dynamic, dynamic>>'.
  player.addEntries({7:"msd",1:"Rahul"}.entries);     //  *****
  print(player);       // INsertion order Not Preserve
 

 //  update
 // player.update(15, (value) => "Virat Kohli"); //Unhandled exception: Invalid argument (key): Key not in map.: 15
  player.update(18,(data)=>"ViratKohli");
  print(player);
  

  // unmodiFialble

 // var constPlayer=UnmodifiableMapBase(player);  //error :Abstract classes can't be instantiated.

  var constPlayer=UnmodifiableMapView(player);
  print(constPlayer);

  //constPlayer[7]="MSDHONI";  // UNhandled exception:Unsupported operation: Cannot modify unmodifiable map

  print(player);



}