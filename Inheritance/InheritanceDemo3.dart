class Parent{
  int x=10;
  String str="Name";
  //int y=2000;
  get getX =>x;
  get getStr => str;
  //get getY=>y;
  void parentMethod(){
    print(x);
    print(str);
    //print(y);
  }
}
class Child extends Parent{
//  int z=7000;
  int x=20;
  String str="Data";
  get getX=>x;
  get getStr=>str;
//  get getZ=>z;
  void childMethod(){
    print(x);
    print(str);
//    print(z);
  }
}
void main(){
  Child obj=new Child();
  print(obj.getX);
  print(obj.getStr);
 // print(obj.getY);
  obj.parentMethod();

  print(obj.getX);
  print(obj.getStr);
//  print(obj.getZ);
  obj.childMethod();
}