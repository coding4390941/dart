

abstract class Developer{
  void develop(){
    print("We Build Software");
  }
  void devType();
}
class MobileDev extends Developer{
  void devType(){
    print("Flutter Developer");
  }
}
class WebDev extends Developer{
  void devType(){
    print("FrontEnd Developer");
  }
}

void main(){
  Developer obj =new MobileDev();
  obj.develop();
  obj.devType();

  Developer obj1=new WebDev();

  
  obj1.develop();
  obj1.devType();

   //   Developer obj=new Developer();
}