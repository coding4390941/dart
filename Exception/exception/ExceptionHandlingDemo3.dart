//

import 'dart:io';

void main(){
  
  print("Start main");
  print("Enter Value:");
  try{
    int? x=int.parse(stdin.readLineSync()!);
    print(2~/x);
  } on FormatException {
    print("Format-Exception occured");
  }on IntegerDivisionByZeroException{
    print("IN INTEGER_DIVISIBLEBYZEROEXCEPTION");
  }catch (amar){
    print("Exception Occured");  // -->
    print(amar);  // FormatException: Invalid radix-10 number (at character 1)
  }

  print("End Main");
}


/*
import 'dart:io';

void main(){

  print("Start main");
  print("Enter Value:");
  try{
    int? x=int.parse(stdin.readLineSync()!);
    print(2~/x);
  }catch (ex){
    print("IN 1ST CATCH BLOCK");
  } on FormatException {
    print("Format-Exception occured");
  }on IntegerDivisionByZeroException{
    print("IN INTEGER_DIVISIBLEBYZEROEXCEPTION");
  }catch (amar){
    print("Exception Occured");  // -->
    print(amar);  // FormatException: Invalid radix-10 number (at character 1)
  }

  print("End Main");
}

*/