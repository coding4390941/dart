
class Company{
  String? compName;
  String? compLoc;
  Company(this.compName,this.compLoc);
  void compInfo(){
    print(compName);
    print(compLoc);
  }
}
class Employee extends Company{

  int ?empId;
  String? empName;
  Employee(this.empId,this.empName,String compName,String compLoc):super( compName,compLoc);

  void empInfo(){
    print(empId);
    print(empName);
  }
}
void main(){
Employee  obj=new Employee(7, "Amar","Google","Pune");
obj.empInfo();
obj.compInfo();
}