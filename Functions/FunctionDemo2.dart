//     Named Argument
void fun({String? name,double? sal}){
print("IN FUN");
print(name);
print(sal);
}
void main(){
  print("Start Main");
  fun(name:"Amar",sal:20.9);   // 2 positional arguments expected by 'fun', but 0 found.
  print("End Main");
}