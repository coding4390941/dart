abstract class IFC{
  void material(){
    print("Indian Material");
  }
  void taste();
}
class IndFC extends IFC{
  void material(){
    print("Indian MAterial:");
  }
  void taste(){
    print("Indian Taste");
  }
}

class EUFC extends IFC{
  void material(){
    print("Indian Material");
  }
  void taste(){
    print("EU taste");
  }
}

void main(){
  IndFC obj=new IndFC();
  obj.material();
  obj.taste();
  
  EUFC obj1=new EUFC();
  obj1.material();
  obj1.taste();
}