import 'SingleTonDesignPatternNormal.dart';

void main(){

 Developer obj1=Developer.devLang();
 Developer obj2=Developer.devLang();

 print(obj1.hashCode);
 print(obj2.hashCode);
 
}