class Parent{
int x=10;
String str="name";
void parentDisp(){
  print(x);
  print(str);
}
} 
class Child extends Parent{
  int y=20;
  String str1="Data";
  void childDisp(){
    print(y);
    print(str1);

  }
}

void main(){
//  Parent obj=new Parent();
//  print(obj.y); //The getter 'y' isn't defined for the class 'Parent'.
//  print(obj.str1); // The getter 'str1' isn't defined for the class 'Parent'.
//  obj.childDisp();  //The method 'childDisp' isn't defined for the class 'Parent'.

  Child obj1=new Child();
  print(obj1.x);  // 10
  print(obj1.str); // Name
  obj1.parentDisp(); // 10 Name

  print(obj1.y); // 20
  print(obj1.str1);  // Data
  obj1.childDisp(); // 20  Data

}
