//   MULTILEVEL
/*class ICC{
  ICC(){
    print("IN ICC CONSTRUCTOR");
  }
}
class BCCI extends ICC{
  BCCI(){
    print("IN BCCI CONSTRUCTOR:");

  }
}
class IPL extends BCCI{
  IPL(){
    print("IN IPL CONSTRUCTOR");
  }
}
void main(){
  IPL obj=new IPL();
} */

// HIRARCHICAL INHERITANCE
class Parent{
  Parent(){
    print("IN parent");
  }
}
class Child1 extends Parent{
  Child1(){
    print("IN Child1:");
  }
}
class Child2 extends Parent{
  Child2(){
    print("IN Child2");
  }
}
void main(){
//  Child1 obj =new Child1();
//  Child2 obj2=new Child2();

}