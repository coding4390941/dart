class Parent{
  int x=10;
  String str="Name";
  int z=50;
  get getX =>x;
  get getStr => str;

  void parentMethod(){
    print(x);
    print(str);
    
  }
}
class Child extends Parent{

  int x=20;
  String str="Data";
  get getX=>x;
  get getStr=>str;

  void childMethod(){
    print(x);
    print(str);
  }
}
void main(){
  Parent obj=new Parent();

  Child obj1=new Child();
  print(obj1.getX);    // 20
  print(obj1.getStr); //data
  print (obj1.z);     // 50
  obj1.parentMethod(); // 20 data
  
  // call on parent object
  obj.parentMethod();  // 10  Name
  
 /*
  Parent obj1=new Child();
  print(obj1.getX);    // 20
  print(obj1.getStr);  //data
  obj1.parentMethod(); // 20 data
*/
}