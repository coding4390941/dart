class Demo{
  int x=10;
  static int y=20;
  void printData(){
    print(x);
    print(y);
  }
}

void main(){
  Demo obj=new Demo();
  obj.printData();

  Demo obj1=Demo();
  obj1.printData();

  obj.x=40;
  //obj1.y=50;  // The Static setter 'y' cant be accessed though an instance
  
  Demo.y=50;

  print(obj.x);
  //print(obj.y);  // The Static Getter 'y' cant be accessed through an instance 
  print(obj1.x);
 // print(obj1.y);// The Static Getter 'y' cant be accessed through an instance
}