import 'dart:collection';

void main(){

  var player =SplayTreeMap();
  //1
  player[18]="Virat";
  print(player);
  //2
  player.addAll({45:"Rohit"});
  print(player);
  //3


 // player.addEntries({7:"msd",1:"Rahul"}); // error: The argument type 'Map<int, String>' can't be assigned to the parameter type 'Iterable<MapEntry<dynamic, dynamic>>'.
  player.addEntries({7:"msd",1:"Rahul"}.entries);     //  *****
  print(player);         // Sorting
  
}