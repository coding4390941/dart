/*
class Demo{
  static Demo obj=new Demo();
  Demo(){
    print("In Constructor");
  }
}

void main(){
  Demo obj=new Demo();   //  In Constructor
} */

class Demo{
  // Demo obj2=new Demo();   //  recurssive call 
  
  static  Demo obj = new  Demo();
  static int x = 90;
  Demo(){
    print("In Constructor");
  }
}
void main(){
  Demo obj = new Demo(); // In Constructor   
  print(Demo.x);      // 90 
  print(obj);         // Instance of 'Demo'
  print(Demo.obj);   //  In Constructor ,  Instance of 'Demo'

}