
import 'dart:collection';

final class Company extends LinkedListEntry<Company>{
  
  int empCount;
  String compName;
  double rev;
  Company(this.empCount,this.compName,this.rev);

  @override
  String toString() {

    return "$empCount  $compName  $rev ";
  }
}

void main(){

 // var empId=LinkedList();   // error:  Couldn't infer type parameter 'E'.Tried to infer 'LinkedListEntry<Object?>' for 'E' which doesn't work:  Type parameter 'E' is declared to extend 'LinkedListEntry<E>' producing 'LinkedListEntry<LinkedListEntry<Object?>>'.

var compInfo=LinkedList<Company>();

compInfo.add(new Company(700, "Veritas", 1000));
compInfo.add(new Company(1200, "Cummins", 1500));
compInfo.add(new Company(1000, "VMWare", 1400));


  print(compInfo);

}