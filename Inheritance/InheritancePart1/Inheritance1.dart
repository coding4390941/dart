class Parent{
  Parent(){
    print("Parent Constructor");
 }
 call(){
    print("In Call Method");
 }
}
class Child extends Parent{
  Child(){
    super();
    print("Child Constructor");
    }
  }

  void main(){
    Child obj=new Child();
    obj(); // in call method
  }
