abstract class Parent{

  void property(){
    print("Gold,Bunglow,Flats,cars");
  }
  void marry();
  void career();
}

class Child extends Parent{

  void career(){
    print("Youtuber");
  }
  void marry(){
    print("Dipika");
  }
}

void main(){
  Parent obj=new Child();

  obj.property();
  obj.career();
  obj.marry();
}