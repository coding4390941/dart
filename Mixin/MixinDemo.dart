
 mixin  DemoParent {   // mixin OR mixin class 
  void m1(){
    print("in m1 Demo Parent");
  }
}
class Demo { 
  void m2(){
    print("In m2 Demo");
  }
}

class DemoChild extends Demo with DemoParent{

}

void main() {
  DemoChild obj =new DemoChild();
  obj.m1();
  obj.m2();
}