
import '../Map/MapDemo.dart';

void main(){

var players=["Rohit","Shubman","Virat","KLRahul","Shreyash","Hardik"];

// 1. any(bool test(E element)) ->bool
//Checks whether any element of this iterable satisfies test.

var retVal=players.any((player) => player[0]=="B");    // 
var retVal2=players.any((player)=>player.length<=5);   //

print(retVal);
print(retVal2);

// 2. contains(Object? element) -> bool
//Whether the collection contains an element equal to element.

var retVal3=players.contains("Shreyash");

print(retVal3);

// 3. elementAt(int index) -> E
//Returns the indexth element.

var retVal4=players.elementAt(5);
print(retVal4);  // Hardik

// 4. every(bool test(E element)) -> bool
//Checks whether every element of this iterable satisfies test.

var retVal5 = players.every((player) => player[0]=="R");
var retVal6 =players.every((element) => element.length >4);

print(retVal5);  // false
print(retVal6); //  true


// 5. firstWhere(bool test(E element),{E or Else()?})  ->E
//The first element that satisfies the given predicate test.

var retVal7=players.firstWhere((data) => data[0]=="S"); // Shubhman
var retVal8=players.firstWhere((data) => data.length==5);  // Rohit

print(retVal7);  // Shubhman
print(retVal8); //  Rohit

// 6. lastWhere(bool test(E element),{E or Else()?})  ->E
// The last element that satisfies the given predicate test.

var retVal9=players.lastWhere((data) => data[0]=="S"); // Shreyash
var retVal10=players.lastWhere((data) => data.length==5);  // Virat

print(retVal9);  // Shreyash
print(retVal10); //  Virat


// 7. fold<T>(T initialValue, T combine(T previousValue, E element )) -> T
// 7. fold<T>( initialValue, test(previousValue,element)) -> T
//Reduces a collection to a single value by iteratively combining each element of the collection with an existing value

var initVal="";
var retVal01 = players.fold(initVal, (prevVal,player) => prevVal+player);

print(retVal01);  //  RohitShumbhmanViratKLRahulShreyashHardik
print(retVal01.runtimeType);   // String 

// 8.  followedBy(Iterable <E> othr) -> Iterable<E>
// Creates the lazy concatenation of this iterable and other

var retVal02=players.followedBy(["Ravindra","Bumrah"]);

print(retVal02);  // (Rohit, Shumbhman, Virat, KLRahul, Shreyash, Hardik, Ravindra, Bumrah)
print(players);   // no change in orignal list


// 9. forEach(void action(E element)) → void
// Invokes action on each element of this iterable in iteration order.

 players.forEach(print);  // print all
// var retVal03 = players.forEach(print);  // print all
//print(retVal03);  // void cant be print


// 10. join([String separator = ""]) → String
// Converts each element to a String and concatenates the strings. 

var retVal04=players.join("(@@)");
print(retVal04);

// 11. map<T>(T toElement(E e)) → Iterable<T>               //  *****
// The current elements of this iterable modified by toElement.

var retVal05=players.map((player) => player+"Bhai"); // (RohitBhai, ShumbhmanBhai, ViratBhai, KLRahulBhai, ShreyashBhai, HardikBhai)

print(retVal05);

// 12. reduce(E combine(E value, E element)) → E
// Reduces a collection to a single value by iteratively combining elements of the collection using the provided function.

var retVal06=players.reduce((data, element) => data+element); 
print(retVal06); // RohitShumbhmanViratKLRahulShreyashHardik

// 13. singleWhere(bool test(E element), {E orElse()?}) → E
// The single element that satisfies test.

//var retVal07=players.singleWhere((element) => element.length==5); // (Compalsory only one time satisfy condition) Unhandled exception:  Bad state: Too many elements
 var retVal07=players.singleWhere((element) => element.length==6);
print(retVal07); // Hardik

// 14.  skip(int count) → Iterable<E>
// Creates an Iterable that provides all but the first count elements.

var retVal08=players.skip(4);  // skip upto 4 and remaining print all
print(retVal08);  // (Shreyash, Hardik)

// 15.skipWhile(bool test(E value)) → Iterable<E>
// Creates an Iterable that skips leading elements while test is satisfied.  (leading means )

var retVal09=players.skipWhile((value) => value.length==5); // (Shubman, Virat, KLRahul, Shreyash, Hardik)
var retVal001=players.skipWhile((value) => value[0]=="R");  //(Shubman, Virat, KLRahul, Shreyash, Hardik)
var retVal002=players.skipWhile((value) => value[0]=="V");  // no change ==> (Rohit, Shubman, Virat, KLRahul, Shreyash, Hardik)

print(retVal09); // (Shubman, Virat, KLRahul, Shreyash, Hardik)
print(retVal001); //(Shubman, Virat, KLRahul, Shreyash, Hardik)
print(retVal002);// (Rohit, Shubman, Virat, KLRahul, Shreyash, Hardik)

// 16.take(int count) → Iterable<E>
// Creates a lazy iterable of the count first elements of this iterable.

var retVal003=players.take(5);
print(retVal003);  // (Rohit, Shubman, Virat, KLRahul, Shreyash)


// 17. takeWhile(bool test(E value)) → Iterable<E>    //  (from first checkes if 1st condition false it will return,if above condition are true  it false because at first it was false)
// Creates a lazy iterable of the leading elements satisfying test.

var retVal004=players.takeWhile((value) => value[0]=="R"); // (Rohit)
print(retVal004);  //(Rohit)

players[1]="Ravindra";
var retVal005=players.takeWhile((value) => value[0]=="R");

print(retVal005);  //(Rohit, Ravindra)

players.add("RaviAshwin");
var retVal006=players.takeWhile((value) => value[0]=="R");
print(retVal006);  // (Rohit, Ravindra)

players.remove("RaviAshwin");
print(players);


// 18. toList({bool growable = true}) → List<E>   *****
 // Creates a List containing the elements of this Iterable.

var retVal007=players.toList();
print(retVal007);  // [Rohit, Ravindra, Virat, KLRahul, Shreyash, Hardik]

// 19. toSet() → Set<E>      *****      // ( to delete duplicate data from list convert to toSet and after that convert to toList in this way duplicate data delete from list )
// Creates a Set containing the same elements as this iterable.

var retVal008=players.toSet();
print(retVal008);     //  {Rohit, Ravindra, Virat, KLRahul, Shreyash, Hardik}


// 20. where(bool test(E element)) → Iterable<E>
// Creates a new lazy Iterable with all elements that satisfy the predicate test.

players[1]="Shubman";
var retVal009=players.where((element) => element[0]=="S");
print(retVal009);  //  (Shubman, Shreyash)


}