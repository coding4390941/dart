
class Backend{
  String? lang;
  Backend._private(String lang){
    if(lang == "JavaScript"){
      this.lang ="NodeJs";
    }
    else if(lang == "Java"){
      this.lang ="SpringBoot";
    }
    else{
      this.lang ="NodeJs/SpringBoot";
    }
  }
     factory Backend(String lang){
            return Backend._private(lang);
    }
}