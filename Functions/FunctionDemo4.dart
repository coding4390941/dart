
// NULL SAFETY
void main(){
  int? age=null;
  String? name="Virat";

  print(age);
  print(name);

  age=35;
  name=null;
  print(age);
  print(null);
}