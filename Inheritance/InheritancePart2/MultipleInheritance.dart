abstract class InterfaceDemo1{
  void fun1(){
    print("In fun1");
  }
}
abstract class InterfaceDemo2{
  void fun2(){
    print("In fun2");
  }
}

class Demo implements InterfaceDemo1,InterfaceDemo2{

  void fun1(){
    print("In demo fun1");
  }
  void  fun2(){
    print("In Demo Fun2");
  //  return 0;
  }
}

void main(){
  Demo obj =new Demo();
  obj.fun1();
  obj.fun2();

}