// filled 

void main(){

  //List player=List.filled(5,1);
  List player=List.filled(5,1,growable: true);

  print(player);  //  [1, 1, 1, 1, 1]
 // player.add(20); //  Unsupported operation: Cannot add to a fixed-length list

  player.add(20);
  player.add(30);
  
  print(player);
}