/*
Constructor in List
1. empty
2. filled
3. from
4. generate
5. of
6. unmodifiable

*/
void main(){

List player=List.empty();
List player1=List.empty(growable: true);

 //player.add("Virat");   //Unsupported operation: Cannot add to a fixed-length list
//player1[0]="Virat";  //RangeError (index): Invalid value: Valid value range is empty: 0

print(player);  // []
print(player1); //  []

player1.add("Virat");
player1.add("Rohit");
print(player1); //     [Virat, Rohit]
player1[0]="Shubhman";
print(player1);   //    [Shubhman, Rohit]

}