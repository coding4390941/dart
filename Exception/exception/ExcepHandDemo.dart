import 'dart:io';

void main(){

  print("Start main");
  print("Enter Value:");
  int?x;
  int ? ans;

  try{
    x=int.parse(stdin.readLineSync()!);
    ans= 5~/x;
  } on IntegerDivisionByZeroException {
    print("Re Enter Value: ");
    x=int.parse(stdin.readLineSync()!);
    ans = 5~/x;
  } on FormatException{
    print("Format Exception");
  } catch (amar){
    print(amar);  // FormatException: Invalid radix-10 number (at character 1)
  }
  print(ans);
  print("End Main");
}