class Parent{
  void m1(){
    print("In Parent m1");
  }
}
mixin Demo1 on Parent{
  void m1(){
    print("In Demo -Fun1");
  }
}
// class Normal with Demo1{}   //Error: 'Object' doesn't implement 'Parent' so it can't be used with 'Demo1'.
class Normal extends Parent with Demo1{

}

void main(){
  Normal obj =new Normal();
  obj.m1();
//  obj.fun1();  //Error: The method 'fun1' isn't defined for the class 'Normal'.
}