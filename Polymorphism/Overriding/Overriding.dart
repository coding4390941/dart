class Parent{
  void career(){
    print("Engineering");
  }
  void mary(){
    print("Dipika Padukone");
  }
}

class Child extends Parent{
  void marry(){
    print("Disha Patani");
  }
}

void main(){
  Child obj=new Child();
  obj.career();
  obj.marry();
}