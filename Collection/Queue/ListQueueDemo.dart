import 'dart:collection';

void main(){

  var compData=ListQueue();     // By Default Implementation of Queue 

  compData.add("MicroSoft");
  compData.add("Amazon");
  compData.add("Google");

  print(compData);
  print(compData.runtimeType);   // ListQueue<dynamic>

  compData.addFirst("Apple");
  compData.addLast("Veritas");

  print(compData);

  compData.removeLast();
  print(compData);

}