mixin Demo1{
  void m1(){
    print("In Demo1 -m1");
  }
}
mixin Demo2{
  void m1(){
    print("In Demo2 -m1");
  }
}
class DemoChild with Demo1,Demo1{   // right thumb rule right side method call

}
void main(){
  DemoChild obj = new DemoChild();
  obj.m1();   
}