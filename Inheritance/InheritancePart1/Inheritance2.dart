class Parent{
  int? x;
  String? str;
  
  Parent(this.x,this.str);
  void printData(){
    print(x);
    print(str);
  }
}
class Child extends Parent{
  int? y;
  String? name;

    Child(this.y,this.name);  // Error :The implicitly invoked unnamed constructor from 'Parent' has required parameters.
   //Try adding an explicit super parameter with the required arguments.
  /*Child(this.y,this.name) : super( 0, ''){
    
  }*/

  void getData(){
    print(y);
    print(name);
  }
}

void main(){
  Child obj=new Child(18,"Virat");
  obj.printData();
  obj.getData();
}