
abstract  mixin class Demo{

  void fun1(){
    print("In Fun1");
  }
  void fun2();
}
class Asach{
  void ashi(){
    print("In Ashi method");
  }
}
class Child extends Asach with Demo{
  void fun2(){
    print("In Fun 2");
  }  
}
void main(){
  Demo obj = new Child();
  obj.fun1();
  obj.fun2();

//  obj.ashi(); //Error: The method 'ashi' isn't defined for the class 'Demo'.

}