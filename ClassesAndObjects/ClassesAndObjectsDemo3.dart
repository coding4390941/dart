import 'dart:io';

class Bharat{

  String? stateName;
  num? statePopu;
  double? stateGdp;

  void stateInfo(){
    print("State Name: $stateName");
    print("State Population: $statePopu");
    print("State GDP: $stateGdp");
  }
}

void main(){
  Bharat bt=new Bharat();

  print("Enter the Name Of The State:");
  bt.stateName=stdin.readLineSync();
  print("Enter the Population Of The State:");
  bt.statePopu=num.parse(stdin.readLineSync()!);
  print("Enter the GDP Of The State:");
  bt.stateGdp=double.parse(stdin.readLineSync()!);

  bt.stateInfo();

}