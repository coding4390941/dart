import 'dart:io';

void main(){

  print("Start main");
  print("Enter Value:");
  try{
    int? x=int.parse(stdin.readLineSync()!);
    print(x);
  } on FormatException {
    print("Format-Exception occured");
  }catch (amar){
    print("Exception Occured");  // -->
    print(amar);  // FormatException: Invalid radix-10 number (at character 1)
  }/*catch(amar){            // no matter because 1st catch execute and exit
     print("In Second Catch block");
  }  */

  print("End Main");
}