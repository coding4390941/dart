import 'dart:io';

class Employee{
  String? empName;//="Abhishek";
  int? empId;//=25;
  double? sal;//=7.5;

  void empInfo(){
    print(empName);
    print(empId);
    print(sal);
  }
}

void main(){
  Employee obj=new Employee();
  obj.empInfo();
  
  print("Enter Employee Id:");

  obj.empId=int.parse(stdin.readLineSync()!);
  print("Enter Employee Name:");
  obj.empName=stdin.readLineSync();
  print("Enter Employee Salary:");
  obj.sal=double.parse(stdin.readLineSync()!);

  obj.empInfo();

}